package controller;

import model.*;
import view.DwellingView;

import java.util.ArrayList;

public class Rieltor {
    private final static String typeFlat = "Flat";
    private final static String typeHouse = "House";
    private final static String typePenthouse = "Penthouse";
    private Catalogue catalogue = new Catalogue();
    private DwellingView dwellingView = new DwellingView();
    private ArrayList<Flat> flats = catalogue.getFlats();
    private ArrayList<House> houses = catalogue.getHouses();
    private ArrayList<Penthouse> penthouses = catalogue.getPenthouses();

    public final void startView() {
        switch (dwellingView.chooseOperation()) {
            case 1:
                createListByDwellingType();
                break;
            case 2:
                createListByMaxCost(dwellingView.getMaxCost());
                break;
            case 3:
                createListByDistanceToSchool(dwellingView.getMaxDistanceToSchool());
            default:
                createListByDwellingType();
        }
    }

    public final void createListByDwellingType() {
        int dwellingType = dwellingView.getDwellingType();
        switch (dwellingType) {
            case 1:
                getCatalogueFlats();
                break;
            case 2:
                getCatalogueHouses();
                break;
            case 3:
                getCataloguePenthouses();
                break;
            default:
                getCatalogueFlats();
        }
    }

    public final void createListByMaxCost(int maxCost) {
        for (Flat flat : flats) {
            if (flat.getCostOfRenting() <= maxCost) {
                dwellingView.prindDetatils(flat.toString(), typeFlat);
            }
        }
        for (House house : houses) {
            if (house.getCostOfRenting() <= maxCost) {
                dwellingView.prindDetatils(house.toString(), typeHouse);
            }
        }
        for (Penthouse penthouse : penthouses) {
            if (penthouse.getCostOfRenting() <= maxCost) {
                dwellingView.prindDetatils(penthouse.toString(), typePenthouse);
            }
        }
    }

    public final void createListByDistanceToSchool(int maxDistance) {
        for (Flat flat : flats) {
            if (flat.getDinstanceToSchool() <= maxDistance) {
                dwellingView.prindDetatils(flat.toString(), typeFlat);
            }
        }
        for (House house : houses) {
            if (house.getDinstanceToSchool() <= maxDistance) {
                dwellingView.prindDetatils(house.toString(), typeHouse);
            }
        }
        for (Penthouse penthouse : penthouses) {
            if (penthouse.getDinstanceToSchool() <= maxDistance) {
                dwellingView.prindDetatils(penthouse.toString(), typePenthouse);
            }
        }
    }

    public final void getCatalogueFlats() {
        for (Flat flat : flats) {
            dwellingView.prindDetatils(flat.toString(), typeFlat);
        }
    }

    public final void getCatalogueHouses() {
        for (House house : houses) {
            dwellingView.prindDetatils(house.toString(), typeHouse);
        }
    }

    public final void getCataloguePenthouses() {
        for (Penthouse penthouse : penthouses) {
            dwellingView.prindDetatils(penthouse.toString(), typePenthouse);
        }
    }
}
