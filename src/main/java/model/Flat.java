package model;

public class Flat extends Dwelling {
    private int floor;

    public Flat(String address, int costOfRenting, int area, int numberOfRooms, int dinstanceToSchool,
                int dinstanceToKindergarten, int floor) {
        super(address, costOfRenting, area, numberOfRooms, dinstanceToSchool, dinstanceToKindergarten);
        this.floor = floor;
    }

    public final int getFloor() {
        return floor;
    }

    @Override
    public String toString() {
        return "Address: " + getAddress() + " Cost: " + getCostOfRenting() +
                " Area: " + getArea() + " Floor: " + floor + " Number of rooms: " +
                getNumberOfRooms() + " Distance to kindergarten: " + getDinstanceToKindergarten();
    }
}
