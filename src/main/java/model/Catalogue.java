package model;

import java.util.ArrayList;

public class Catalogue {
    private ArrayList<Flat> flats = new ArrayList<Flat>();
    private ArrayList<House> houses = new ArrayList<House>();
    private ArrayList<Penthouse> penthouses = new ArrayList<Penthouse>();

    public Catalogue() {
        addFlats();
        addHouses();
        addPenthouses();
    }

    public final ArrayList<Flat> getFlats() {
        return flats;
    }

    public final ArrayList<House> getHouses() {
        return houses;
    }

    public final ArrayList<Penthouse> getPenthouses() {
        return penthouses;
    }

    private void addFlats() {
        Flat flat1 = new Flat("Kyiv", 300, 40,
                2, 3, 5, 8);
        Flat flat2 = new Flat("Lviv", 800, 90,
                3, 1, 1, 4);
        Flat flat3 = new Flat("Lviv", 500, 60,
                2, 5, 1, 2);
        flats.add(flat1);
        flats.add(flat2);
        flats.add(flat3);
    }

    private void addHouses() {
        House house1 = new House("Ternopil", 3000, 260, 6,
                10, 5, 2, "Garage, swimming pool");
        House house2 = new House("Lviv", 800, 150, 4,
                12, 5, 1, "Garage");
        houses.add(house1);
        houses.add(house2);
    }

    private void addPenthouses() {
        Penthouse penthouse1 = new Penthouse("Lviv", 900, 100, 4,
                3, 2, 14, "Private entrance");
        Penthouse penthouse2 = new Penthouse("Lviv", 1200, 200, 5,
                4, 1, 11, "Terrace, oversized windows");
        penthouses.add(penthouse1);
        penthouses.add(penthouse2);
    }
}
