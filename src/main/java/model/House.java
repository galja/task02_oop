package model;

public class House extends Dwelling {
    private int numberOfStoreys;
    private String amenities;

    public House(String address, int costOfRenting, int area, int numberOfRooms, int dinstanceToSchool,
                 int dinstanceToKindergarten, int numberOfStoreys, String amenities) {
        super(address, costOfRenting, area, numberOfRooms, dinstanceToSchool, dinstanceToKindergarten);
        this.numberOfStoreys = numberOfStoreys;
        this.amenities = amenities;
    }

    public final int getNumberOfStoreys() {
        return numberOfStoreys;
    }

    public final String getAmenities() {
        return amenities;
    }

    @Override
    public String toString() {
        return "Address: " + getAddress() + "Cost: " + getCostOfRenting() +
                "Area: " + getArea() + "Number of storeys: " + numberOfStoreys+ "Amentities:"+amenities;
    }
}
