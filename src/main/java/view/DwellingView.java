package view;

import java.util.InputMismatchException;
import java.util.Scanner;

public class DwellingView {
    private final static String MENU = "Choose operation \n1 - type of dwelling \n2 - price\n3 - distance to school";
    private final static String MESSAGE_TYPE_OF_ACCOMMODATION = "Choose type of accommodation\n Flat - 1\n House - 2\nPenthouse - 3";

    public final int chooseOperation() {
        return scanAnInteger(MENU);
    }

    public final int getDwellingType() {
        return scanAnInteger(MESSAGE_TYPE_OF_ACCOMMODATION);
    }

    public final int getMaxCost() {
        return scanAnInteger("Enter max cost");
    }

    public final int getMaxDistanceToSchool() {
        return scanAnInteger("Enter max distance to school");
    }

    private final int scanAnInteger(String message) {
        Scanner in = new Scanner(System.in);
        int value = 0;
        System.out.println(message);
        try {
            value = in.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Enter an integer, please");
        }
        return value;
    }

    public final void prindDetatils(String message, String type) {
        System.out.println(type + "\t" + message);
    }
}
